package nitrado_service_v2_client

import "fmt"

// CreateNitradoTokenRequest struct
type CreateNitradoTokenRequest struct {
	NitradoToken string   `json:"nitrado_token"`
	AccountNames []string `json:"account_names"`
}

// CreateNitradoTokenResponse struct
type CreateNitradoTokenResponse struct {
	Message         string `json:"message"`
	NitradoTokenKey string `json:"nitrado_token_key"`
	Error           string `json:"error,omitempty"`
}

// UpdateNitradoTokenRequest struct
type UpdateNitradoTokenRequest struct {
	NitradoToken string             `json:"nitrado_token"`
	Accounts     map[string][]int64 `json:"accounts"`
}

// UpdateNitradoTokenResponse struct
type UpdateNitradoTokenResponse struct {
	Message         string `json:"message"`
	NitradoTokenKey string `json:"nitrado_token_key"`
	Error           string `json:"error,omitempty"`
}

// CreateNitradoToken func
func (c *Client) CreateNitradoToken(body CreateNitradoTokenRequest) (r CreateNitradoTokenResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/nitrado-tokens", base)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
	}

	rErr := Request(&r, "POST", url, nil, body, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}

// UpdateNitradoToken func
func (c *Client) UpdateNitradoToken(body UpdateNitradoTokenRequest) (r UpdateNitradoTokenResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/nitrado-tokens", base)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
	}

	rErr := Request(&r, "PUT", url, nil, body, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}
