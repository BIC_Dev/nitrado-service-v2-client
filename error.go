package nitrado_service_v2_client

// ClientError struct
type ClientError struct {
	status  int
	message string
	err     error
}

// Error func
func (e *ClientError) Error() string {
	return e.err.Error()
}

// Message func
func (e *ClientError) Message() string {
	return e.message
}

// Status func
func (e *ClientError) Status() int {
	return e.status
}

// ErrorResponse response struct for an error
type ErrorResponse struct {
	StatusCode int    `json:"-"`
	Error      string `json:"error"`
	Message    string `json:"message"`
}
