package nitrado_service_v2_client

import "fmt"

// GetServicesResponse struct
type GetLogsResponse struct {
	Message    string      `json:"message"`
	PlayerLogs []PlayerLog `json:"player_logs"`
	AdminLogs  []AdminLog  `json:"admin_logs"`
	KillLogs   []KillLog   `json:"kill_logs"`
	Error      string      `json:"error,omitempty"`
}

// PlayerLog struct
type PlayerLog struct {
	Gamertag  string `json:"gamertag"`
	Name      string `json:"name"`
	Message   string `json:"message"`
	Timestamp int64  `json:"timestamp"`
}

// AdminLog struct
type AdminLog struct {
	Name      string `json:"name"`
	Command   string `json:"command"`
	Timestamp int64  `json:"timestamp"`
}

// KillLog struct
type KillLog struct {
	PvEKill        bool   `json:"pve_kill"`
	KilledName     string `json:"killed_name"`
	KilledLevel    int    `json:"killed_level"`
	KilledDinoType string `json:"killed_dino_type"`
	KilledTribe    string `json:"killed_tribe"`
	KillerName     string `json:"killer_name"`
	KillerLevel    int    `json:"killer_level"`
	KillerDinoType string `json:"killer_dino_type"`
	KillerTribe    string `json:"killer_tribe"`
	Timestamp      int64  `json:"timestamp"`
}

// GetLogsfunc
func (c *Client) GetLogs(accountName string, serverID string, chatLogs bool, adminLogs bool, killLogs bool, cached bool) (r GetLogsResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/logs/%s", base, serverID)

	params := map[string]string{
		"chatlog":  fmt.Sprint(chatLogs),
		"adminlog": fmt.Sprint(adminLogs),
		"killlog":  fmt.Sprint(killLogs),
	}

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}
	if !cached {
		headers["Cache-Control"] = "no-cache"
	}

	rErr := Request(&r, "GET", url, params, nil, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}
