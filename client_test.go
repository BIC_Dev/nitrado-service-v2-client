package nitrado_service_v2_client

import (
	"testing"
)

// TestNewClient func
func TestNewClient(t *testing.T) {
	ccUnset := ClientConfig{
		ServiceToken: "",
	}

	_, cErr := NewClient(ccUnset)

	if cErr == nil {
		t.Error("NewClient failed, expected error response for empty Service Token in Client Config, got nil error")
	} else {
		t.Log("NewClient success, expected error response, got error response")
	}

	cc := ClientConfig{
		ServiceToken: "test-service-token",
	}

	client, cErr2 := NewClient(cc)

	if cErr2 != nil {
		t.Error("NewClient failed, expected Client response, got error instead")
	} else if client.ServiceToken == "" {
		t.Error("NewClient failed, expected Client response, got Client response with no ServiceToken set")
	} else {
		t.Log("NewClient success, expected Client response, got Client response")
	}
}
