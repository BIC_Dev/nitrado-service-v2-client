package nitrado_service_v2_client

import (
	"os"
	"testing"
)

// TestGetServices func
func TestGetGameserver(t *testing.T) {
	client := Client{
		BasePath:     os.Getenv("BASE_PATH"),
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
	}

	response, err := client.GetGameserverByID(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), false)

	if err != nil {
		t.Errorf("GetGameserver failed, expected nil error, got %s : %s", err.Message(), err.Error())
	} else if response.Error != "" {
		t.Errorf("GetGameserver failed, expected no error in response, got %s", response.Error)
	} else if response.Data.Gameserver.ServiceID == 0 {
		t.Errorf("GetGameserver failed, expected non 0 service id, got %d", response.Data.Gameserver.ServiceID)
	} else {
		t.Logf("GetGameserver success, expected response with gameserver, got response with gameserver ID %d", response.Data.Gameserver.ServiceID)
	}
}

// TestGetBanlist func
func TestGetBanlist(t *testing.T) {
	client := Client{
		BasePath:     os.Getenv("BASE_PATH"),
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
	}

	response, err := client.GetBanlist(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), false)

	if err != nil {
		t.Errorf("GetBanlist failed, expected nil error, got %s : %s", err.Message(), err.Error())
	} else if response.Error != "" {
		t.Errorf("GetBanlist failed, expected no error in response, got %s", response.Error)
	} else if response.Status != "success" {
		t.Errorf("GetBanlist failed, expected status of 'success', got %s", response.Status)
	} else {
		t.Log("GetBanlist success")
	}
}

// TestGetWhitelist func
func TestGetWhitelist(t *testing.T) {
	client := Client{
		BasePath:     os.Getenv("BASE_PATH"),
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
	}

	response, err := client.GetWhitelist(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), false)

	if err != nil {
		t.Errorf("TestGetWhitelist failed, expected nil error, got %s : %s", err.Message(), err.Error())
	} else if response.Error != "" {
		t.Errorf("TestGetWhitelist failed, expected no error in response, got %s", response.Error)
	} else if response.Status != "success" {
		t.Errorf("TestGetWhitelist failed, expected status of 'success', got %s", response.Status)
	} else {
		t.Log("TestGetWhitelist success")
	}
}

// // TestStopGameserver func
// func TestStopGameserver(t *testing.T) {
// 	client := Client{
// 		BasePath:     os.Getenv("BASE_PATH"),
// 		ServiceToken: os.Getenv("SERVICE_TOKEN"),
// 	}

// 	response, err := client.StopGameserver(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), "Stopping gameserver", "Gameserver is shutting down")

// 	if err != nil {
// 		t.Errorf("StopGameserver failed, expected nil error, got %s : %s", err.Message(), err.Error())
// 	} else if response.Error != "" {
// 		t.Errorf("StopGameserver failed, expected no error in response, got %s", response.Error)
// 	} else if response.Data.Status != "success" {
// 		t.Errorf("StopGameserver failed, expected status of 'success', got %s", response.Data.Status)
// 	} else {
// 		t.Log("StopGameserver success")
// 	}
// }

// // TestRestartGameserver func
// func TestRestartGameserver(t *testing.T) {
// 	client := Client{
// 		BasePath:     os.Getenv("BASE_PATH"),
// 		ServiceToken: os.Getenv("SERVICE_TOKEN"),
// 	}

// 	response, err := client.RestartGameserver(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), "Stopping gameserver", "Gameserver is shutting down")

// 	if err != nil {
// 		t.Errorf("RestartGameserver failed, expected nil error, got %s : %s", err.Message(), err.Error())
// 	} else if response.Error != "" {
// 		t.Errorf("RestartGameserver failed, expected no error in response, got %s", response.Error)
// 	} else if response.Status != "success" {
// 		t.Errorf("RestartGameserver failed, expected status of 'success', got %s", response.Status)
// 	} else {
// 		t.Log("RestartGameserver success")
// 	}
// }
