package nitrado_service_v2_client

import (
	"os"
	"testing"
)

// TestGetServices func
func TestGetServices(t *testing.T) {
	client := Client{
		BasePath:     os.Getenv("BASE_PATH"),
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
	}

	response, err := client.GetServices(os.Getenv("ACCOUNT_NAME"))

	if err != nil {
		t.Errorf("GetServices failed, expected nil error, got %s : %s", err.Message(), err.Error())
	} else if response.Error != "" {
		t.Errorf("GetServices failed, expected no error in response, got %s", response.Error)
	} else if len(response.Services) == 0 {
		t.Errorf("GetServices failed, expected more than 0 services in response, got %d services", len(response.Services))
	} else {
		t.Logf("GetServices success, expected response with services, got response with %d services", len(response.Services))
	}
}
