package nitrado_service_v2_client

import "fmt"

// GetServicesResponse struct
type GetPlayersResponse struct {
	Message string   `json:"message"`
	Players []Player `json:"players"`
	Error   string   `json:"error,omitempty"`
}

// SearchPlayersRequest struct
type SearchPlayersRequest struct {
	PlayerName string `json:"player_name"`
}

// SearchPlayersResponse struct
type SearchPlayersResponse struct {
	Message string   `json:"message"`
	Players []Player `json:"players"`
	Error   string   `json:"error,omitempty"`
}

// BanPlayerRequest struct
type BanPlayerRequest struct {
	PlayerID string `json:"player_id"`
}

// BanPlayerResponse struct
type BanPlayerResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Error   string `json:"error,omitempty"`
}

// UnbanPlayerRequest struct
type UnbanPlayerRequest struct {
	PlayerID string `json:"player_id"`
}

// UnbanPlayerResponse struct
type UnbanPlayerResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Error   string `json:"error,omitempty"`
}

// WhitelistPlayerRequest struct
type WhitelistPlayerRequest struct {
	PlayerID string `json:"player_id"`
}

// WhitelistPlayerResponse struct
type WhitelistPlayerResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Error   string `json:"error,omitempty"`
}

// UnwhitelistPlayerRequest struct
type UnwhitelistPlayerRequest struct {
	PlayerID string `json:"player_id"`
}

// UnwhitelistPlayerResponse struct
type UnwhitelistPlayerResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Error   string `json:"error,omitempty"`
}

// Players struct
type Player struct {
	Name       string   `json:"name"`
	ID         string   `json:"id"`
	IDType     string   `json:"id_type"`
	Online     bool     `json:"online"`
	Actions    []string `json:"actions"`
	LastOnline string   `json:"last_online"`
}

// GetPlayers func
func (c *Client) GetPlayers(accountName string, serverID string, online bool, cached bool) (r GetPlayersResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/players/%s", base, serverID)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}
	if !cached {
		headers["Cache-Control"] = "no-cache"
	}

	var params map[string]string = make(map[string]string, 0)
	if online {
		params["online"] = "true"
	}

	rErr := Request(&r, "GET", url, params, nil, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}

// SearchPlayers func
func (c *Client) SearchPlayers(accountName string, serverID string, playerName string, exactMatch bool, online bool, cached bool) (r SearchPlayersResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/players/%s/search", base, serverID)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}
	if !cached {
		headers["Cache-Control"] = "no-cache"
	}

	var params map[string]string = make(map[string]string, 0)
	if online {
		params["online"] = "true"
	}
	if exactMatch {
		params["exact_match"] = "true"
	}

	rErr := Request(&r, "POST", url, params, SearchPlayersRequest{
		PlayerName: playerName,
	}, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}

// BanPlayer func
func (c *Client) BanPlayer(accountName string, serverID string, playerID string) (r BanPlayerResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/players/%s/ban", base, serverID)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}

	rErr := Request(&r, "POST", url, nil, BanPlayerRequest{
		PlayerID: playerID,
	}, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}

// UnbanPlayer func
func (c *Client) UnbanPlayer(accountName string, serverID string, playerID string) (r UnbanPlayerResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/players/%s/unban", base, serverID)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}

	rErr := Request(&r, "DELETE", url, nil, UnbanPlayerRequest{
		PlayerID: playerID,
	}, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}

// WhitelistPlayer func
func (c *Client) WhitelistPlayer(accountName string, serverID string, playerID string) (r WhitelistPlayerResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/players/%s/whitelist", base, serverID)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}

	rErr := Request(&r, "POST", url, nil, WhitelistPlayerRequest{
		PlayerID: playerID,
	}, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}

// UnwhitelistPlayer func
func (c *Client) UnwhitelistPlayer(accountName string, serverID string, playerID string) (r UnwhitelistPlayerResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/players/%s/whitelist", base, serverID)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}

	rErr := Request(&r, "DELETE", url, nil, UnwhitelistPlayerRequest{
		PlayerID: playerID,
	}, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}
