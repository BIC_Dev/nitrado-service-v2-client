package nitrado_service_v2_client

import "fmt"

// GetServicesResponse struct
type GetServicesResponse struct {
	Message  string    `json:"message"`
	Services []Service `json:"services"`
	Error    string    `json:"error,omitempty"`
}

// Service struct
type Service struct {
	ID                    int         `json:"id"`
	LocationID            int         `json:"location_id"`
	Status                string      `json:"status"`
	WebsocketToken        string      `json:"websocket_token"`
	UserID                int         `json:"user_id"`
	Comment               interface{} `json:"comment"`
	AutoExtension         bool        `json:"auto_extension"`
	AutoExtensionDuration int         `json:"auto_extension_duration"`
	Type                  string      `json:"type"`
	TypeHuman             string      `json:"type_human"`
	ManagedrootID         interface{} `json:"managedroot_id"`
	Details               struct {
		Address       string `json:"address"`
		Name          string `json:"name"`
		Game          string `json:"game"`
		PortlistShort string `json:"portlist_short"`
		FolderShort   string `json:"folder_short"`
		Slots         int    `json:"slots"`
	} `json:"details"`
	StartDate    string   `json:"start_date"`
	SuspendDate  string   `json:"suspend_date"`
	DeleteDate   string   `json:"delete_date"`
	SuspendingIn int      `json:"suspending_in"`
	DeletingIn   int      `json:"deleting_in"`
	Username     string   `json:"username"`
	Roles        []string `json:"roles"`
}

// GetServices func
func (c *Client) GetServices(accountName string) (r GetServicesResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/services", base)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}

	rErr := Request(&r, "GET", url, nil, nil, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}
