package nitrado_service_v2_client

import "fmt"

// GetBoostSettingsResponse struct
type GetBoostSettingsResponse struct {
	Message       string        `json:"message"`
	BoostSettings BoostSettings `json:"boost_settings"`
	Error         string        `json:"error,omitempty"`
}

// BoostSettings struct
type BoostSettings struct {
	Enabled        bool   `json:"enabled"`
	Code           string `json:"code"`
	Message        string `json:"message"`
	WelcomeMessage string `json:"welcome_message"`
}

// GetBoostSettings func
func (c *Client) GetBoostSettings(accountName string, gameServerID int) (r GetBoostSettingsResponse, e *ClientError) {
	base := c.BasePath
	if base == "" {
		base = BasePath
	}

	url := fmt.Sprintf("%s/boost-settings/%d", base, gameServerID)

	headers := map[string]string{
		"Service-Token": c.ServiceToken,
		"Account-Name":  accountName,
	}

	rErr := Request(&r, "GET", url, nil, nil, headers)
	if rErr != nil {
		return r, rErr
	}

	return r, nil
}
