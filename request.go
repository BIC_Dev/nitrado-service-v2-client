package nitrado_service_v2_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// Request handler
func Request(output interface{}, method string, url string, params map[string]string, body interface{}, headers map[string]string) *ClientError {
	httpClient := &http.Client{
		Timeout: time.Second * Timeout,
	}

	var requestBody *bytes.Buffer
	var newRequest *http.Request
	var newRequestErr error

	if body != nil {
		jsonBody, jsonErr := json.Marshal(body)

		if jsonErr != nil {
			return &ClientError{
				status:  http.StatusInternalServerError,
				message: "Could not marshal request struct",
				err:     jsonErr,
			}
		}

		requestBody = bytes.NewBuffer(jsonBody)

		newRequest, newRequestErr = http.NewRequest(method, url, requestBody)
	} else {
		newRequest, newRequestErr = http.NewRequest(method, url, nil)
	}

	if newRequestErr != nil {
		return &ClientError{
			status:  http.StatusInternalServerError,
			message: "Failed to create request",
			err:     newRequestErr,
		}
	}

	if params != nil {
		q := newRequest.URL.Query()

		for key, val := range params {
			q.Add(key, val)
		}

		newRequest.URL.RawQuery = q.Encode()
	}

	for k, v := range headers {
		newRequest.Header.Add(k, v)
	}

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		return &ClientError{
			status:  http.StatusFailedDependency,
			message: "Request failed",
			err:     requestErr,
		}
	}

	switch response.StatusCode {
	case http.StatusOK:
		jsonErr := json.NewDecoder(response.Body).Decode(&output)

		if jsonErr != nil {
			bodyBytes, _ := ioutil.ReadAll(response.Body)
			bodyString := string(bodyBytes)

			return &ClientError{
				status:  http.StatusInternalServerError,
				message: "Bad response from api endpoint",
				err:     fmt.Errorf(bodyString),
			}
		}

		return nil
	default:
		errorResponse := ErrorResponse{}
		jsonErr := json.NewDecoder(response.Body).Decode(&errorResponse)

		if jsonErr != nil {
			bodyBytes, _ := ioutil.ReadAll(response.Body)
			bodyString := string(bodyBytes)

			return &ClientError{
				status:  response.StatusCode,
				message: "Bad response from api endpoint",
				err:     fmt.Errorf(bodyString),
			}
		}

		return &ClientError{
			status:  response.StatusCode,
			message: errorResponse.Message,
			err:     fmt.Errorf(errorResponse.Error),
		}
	}
}
