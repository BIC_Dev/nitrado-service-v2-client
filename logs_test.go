package nitrado_service_v2_client

import (
	"os"
	"testing"
)

// TestGetLogs func
func TestGetLogs(t *testing.T) {
	client := Client{
		BasePath:     os.Getenv("BASE_PATH"),
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
	}

	response, err := client.GetLogs(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), true, true, true, false)

	if err != nil {
		t.Errorf("GetLogs failed, expected nil error, got %s : %s", err.Message(), err.Error())
	} else if response.Error != "" {
		t.Errorf("GetLogs failed, expected no error in response, got %s", response.Error)
	} else {
		t.Log("GetLogs success")
	}
}
