package nitrado_service_v2_client

import (
	"os"
	"testing"
)

// TestGetPlayers func
func TestGetPlayers(t *testing.T) {
	client := Client{
		BasePath:     os.Getenv("BASE_PATH"),
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
	}

	response, err := client.GetPlayers(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), false, false)

	if err != nil {
		t.Errorf("GetPlayers failed, expected nil error, got %s : %s", err.Message(), err.Error())
	} else if response.Error != "" {
		t.Errorf("GetPlayers failed, expected no error in response, got %s", response.Error)
	} else if len(response.Players) == 0 {
		t.Errorf("GetPlayers failed, expected more than 0 players in response, got %d players", len(response.Players))
	} else {
		t.Logf("GetPlayers success, expected response with players, got response with %d players", len(response.Players))
	}
}

// // TestBanPlayer func
// func TestBanPlayer(t *testing.T) {
// 	client := Client{
// 		BasePath:     os.Getenv("BASE_PATH"),
// 		ServiceToken: os.Getenv("SERVICE_TOKEN"),
// 	}

// 	response, err := client.BanPlayer(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), os.Getenv("PLAYER_ID"))

// 	if err != nil {
// 		t.Errorf("BanPlayer failed, expected nil error, got %s : %s", err.Message(), err.Error())
// 	} else if response.Error != "" {
// 		t.Errorf("BanPlayer failed, expected no error in response, got %s", response.Error)
// 	} else if response.Status != "success" {
// 		t.Errorf("BanPlayer failed, status of 'success', got %s", response.Status)
// 	} else {
// 		t.Log("BanPlayer success")
// 	}
// }

// // TestUnbanPlayer func
// func TestUnbanPlayer(t *testing.T) {
// 	client := Client{
// 		BasePath:     os.Getenv("BASE_PATH"),
// 		ServiceToken: os.Getenv("SERVICE_TOKEN"),
// 	}

// 	response, err := client.UnbanPlayer(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), os.Getenv("PLAYER_ID"))

// 	if err != nil {
// 		t.Errorf("UnbanPlayer failed, expected nil error, got %s : %s", err.Message(), err.Error())
// 	} else if response.Error != "" {
// 		t.Errorf("UnbanPlayer failed, expected no error in response, got %s", response.Error)
// 	} else if response.Status != "success" {
// 		t.Errorf("UnbanPlayer failed, status of 'success', got %s", response.Status)
// 	} else {
// 		t.Log("UnbanPlayer success")
// 	}
// }

// // TestWhitelistPlayer func
// func TestWhitelistPlayer(t *testing.T) {
// 	client := Client{
// 		BasePath:     os.Getenv("BASE_PATH"),
// 		ServiceToken: os.Getenv("SERVICE_TOKEN"),
// 	}

// 	response, err := client.WhitelistPlayer(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), os.Getenv("PLAYER_ID"))

// 	if err != nil {
// 		t.Errorf("WhitelistPlayer failed, expected nil error, got %s : %s", err.Message(), err.Error())
// 	} else if response.Error != "" {
// 		t.Errorf("WhitelistPlayer failed, expected no error in response, got %s", response.Error)
// 	} else if response.Status != "success" {
// 		t.Errorf("WhitelistPlayer failed, status of 'success', got %s", response.Status)
// 	} else {
// 		t.Log("WhitelistPlayer success")
// 	}
// }

// // TestUnwhitelistPlayer func
// func TestUnwhitelistPlayer(t *testing.T) {
// 	client := Client{
// 		BasePath:     os.Getenv("BASE_PATH"),
// 		ServiceToken: os.Getenv("SERVICE_TOKEN"),
// 	}

// 	response, err := client.UnwhitelistPlayer(os.Getenv("ACCOUNT_NAME"), os.Getenv("GAME_SERVER_ID"), os.Getenv("PLAYER_ID"))

// 	if err != nil {
// 		t.Errorf("TestUnwhitelistPlayer failed, expected nil error, got %s : %s", err.Message(), err.Error())
// 	} else if response.Error != "" {
// 		t.Errorf("TestUnwhitelistPlayer failed, expected no error in response, got %s", response.Error)
// 	} else if response.Status != "success" {
// 		t.Errorf("TestUnwhitelistPlayer failed, status of 'success', got %s", response.Status)
// 	} else {
// 		t.Log("TestUnwhitelistPlayer success")
// 	}
// }
