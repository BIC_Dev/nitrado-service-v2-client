package nitrado_service_v2_client

import (
	"os"
	"strconv"
	"testing"
)

// TestGetBoostSettings func
func TestGetBoostSettings(t *testing.T) {
	accountName := os.Getenv("ACCOUNT_NAME")
	gameServerID := os.Getenv("GAME_SERVER_ID")
	gsIDInt, intErr := strconv.Atoi(gameServerID)
	if intErr != nil {
		t.Errorf("GetBoostSettings failed, expected int GAME_SERVER_ID, got %s", intErr.Error())
	}
	client := Client{
		BasePath:     os.Getenv("BASE_PATH"),
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
	}

	response, err := client.GetBoostSettings(accountName, gsIDInt)

	if err != nil {
		t.Errorf("GetBoostSettings failed, expected nil error, got %s : %s", err.Message(), err.Error())
	} else if response.Error != "" {
		t.Errorf("GetBoostSettings failed, expected no error in response, got %s", response.Error)
	} else {
		t.Log("GetBoostSettings success, expected response with boost settings, got response with boost settings")
	}
}
