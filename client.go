package nitrado_service_v2_client

import "errors"

// Client struct
type Client struct {
	ServiceToken string
	BasePath     string
}

// ClientConfig struct
type ClientConfig struct {
	BasePath     string
	ServiceToken string
}

// NewClient func
func NewClient(cc ClientConfig) (*Client, error) {
	if cc.ServiceToken == "" {
		return nil, errors.New("missing service token in client config")
	}

	return &Client{
		ServiceToken: cc.ServiceToken,
		BasePath:     cc.BasePath,
	}, nil
}
